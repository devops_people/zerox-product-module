/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kasai
 */




public class Customer {
    
    int customerId;
    private List<Order> order;
    int totalDifference;
    
    
    public Customer(int customerId)
    {
        this.customerId = customerId;
        this.order = new ArrayList<>();
        
      
    }
    
    

    public List<Order> getOrders() {
        return order;
    }

    public void setOrders(List<Order> order) {
        this.order = order;
    }

    public int getTotalDifference() {
        return totalDifference;
    }

    public void setTotalDifference(int totalDifference) {
        this.totalDifference = totalDifference;
    }

   

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }
    
    
     @Override
    public String toString() {
        return "Customer [customerId=" + customerId + ", order=" + order + "]";
    }
}
