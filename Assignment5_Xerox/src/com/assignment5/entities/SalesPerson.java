/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kasai
 */
public class SalesPerson {
    
    int salesId ;
    private List<Order> order;
    int TotalDifference;
    int profit;

    public List<Order> getOrders() {
        return order;
    }

    public void setOrders(List<Order> order) {
        this.order = order;
    }

    public int getTotalDifference() {
        return TotalDifference;
    }

    public void setTotalDifference(int TotalDifference) {
        this.TotalDifference = TotalDifference;
    }

    public int getProfit() {
        return profit;
    }

    public void setProfit(int profit) {
        this.profit = profit;
    }
    
    
    public SalesPerson(int salesId)
    {
        this.salesId = salesId;
        this.order= new ArrayList<>();
    }

    public int getSalesId() {
        return salesId;
    }

    public void setSalesId(int salesId) {
        this.salesId = salesId;
    }
    @Override
    public String toString() {
        return "SalesPerson{" + "salesid = " + salesId  + '}';
    }
    
    
}
