/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;

/**
 *
 * @author kasai
 */
public class Item {
    
    int productId;
    int salesPrice;
    int quantity;
    int itemId;
    int differencePrice;
    int salesId;

    public int getDifferencePrice() {
        return differencePrice;
    }

    public void setDifferencePrice(int differencePrice) {
        this.differencePrice = differencePrice;
    }

//    public Item(int productId, int salesPrice, int quantity, int itemId, int differencePrice, int salesId) {
//        this.productId = productId;
//        this.salesPrice = salesPrice;
//        this.quantity = quantity;
//        this.itemId = itemId;
//        this.differencePrice = differencePrice;
//        this.salesId = salesId;
//    }
//    
    
    
     public Item(int itemId,int productId, int salesPrice, int quantity, int salesId) {
        this.itemId = itemId;
        this.productId = productId;
        this.salesPrice = salesPrice;
        this.quantity = quantity;
        this.salesId= salesId;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getSalesPrice() {
        return salesPrice;
    }

    public void setSalesPrice(int salesPrice) {
        this.salesPrice = salesPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getSalesId() {
        return salesId;
    }

    public void setSalesId(int salesId) {
        this.salesId = salesId;
    }
    
    
    
    
    @Override
  public String toString() {
        return "Item{" + "productId= " +" "+ productId + " " + "salesPrice= "+ salesPrice + " " + "quantity = " +" " + quantity  + " " + "Itemid= " +" "+ itemId+'}';
    } 
    
    
}
