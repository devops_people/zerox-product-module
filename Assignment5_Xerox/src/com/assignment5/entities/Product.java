/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kasai
 */



public class Product {
    
    
    int productId;
    int minPrice;
    int maxPrice;
    int targetPrice;
    private List<Item> items;
    private int sumQuantityOfProductsAboveTarget;
    double modifiedTargetPrice;
    double error;
    double diffAvgAndTargetOriginal;
    double diffAvgAndTargetModified;
    double avgSalesPrice;

    public double getAvgSalesPrice() {
        return avgSalesPrice;
    }

    public void setAvgSalesPrice(double avgSalesPrice) {
        this.avgSalesPrice = avgSalesPrice;
    }
    
    public Product(int productId,int minPrice , int maxPrice , int targetPrice )
    {
        this.productId = productId;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
        this.targetPrice = targetPrice;
        this.items = new ArrayList<>();
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public int getSumQuantityOfProductsAboveTarget() {
        return sumQuantityOfProductsAboveTarget;
    }

    public void setSumQuantityOfProductsAboveTarget(int sumQuantityOfProductsAboveTarget) {
        this.sumQuantityOfProductsAboveTarget = sumQuantityOfProductsAboveTarget;
    }

    public double getModifiedTargetPrice() {
        return modifiedTargetPrice;
    }

    public void setModifiedTargetPrice(double modifiedTargetPrice) {
        this.modifiedTargetPrice = modifiedTargetPrice;
    }

    public double getError() {
        return error;
    }

    public void setError(double error) {
        this.error = error;
    }

    public double getDiffAvgAndTargetOriginal() {
        return diffAvgAndTargetOriginal;
    }

    public void setDiffAvgAndTargetOriginal(double diffAvgAndTargetOriginal) {
        this.diffAvgAndTargetOriginal = diffAvgAndTargetOriginal;
    }

    public double getDiffAvgAndTargetModified() {
        return diffAvgAndTargetModified;
    }

    public void setDiffAvgAndTargetModified(double diffAvgAndTargetModified) {
        this.diffAvgAndTargetModified = diffAvgAndTargetModified;
    }

    
    public int getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(int minPrice) {
        this.minPrice = minPrice;
    }

    public int getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(int maxPrice) {
        this.maxPrice = maxPrice;
    }

    public int getTargetPrice() {
        return targetPrice;
    }

    public void setTargetPrice(int targetPrice) {
        this.targetPrice = targetPrice;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }
    
    public String toString() {
        return "productId{" + "productId= " +" "+ productId + " " + "minPrice= "+ minPrice + " " + "maxPrice = " +" " + maxPrice + " "+ "TargetPrice= " +targetPrice+'}';
    }
    
}
