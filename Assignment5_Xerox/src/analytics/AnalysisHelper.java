/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analytics;

import com.assignment5.entities.Customer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.assignment5.entities.Item;
import com.assignment5.entities.Order;
import com.assignment5.entities.Product;
import com.assignment5.entities.SalesPerson;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Set;

/**
 *
 * @author Aashita
 */
public class AnalysisHelper {
    
    
    public void top3BestNegotiatedProducts(){
       
        Map<Integer,Integer> quantityStorage = new HashMap<>();
        
       Map<Integer, Product> product = DataStore.getInstance().getProducts();
       List<Product> listproduct = new ArrayList<Product>(product.values());
//       System.out.print("");
//       System.out.println(listproduct);
       
       Map<Integer,Item> item = DataStore.getInstance().getItems();
       List<Item> listitem = new ArrayList<Item>(item.values());
        
//         System.out.print("");
//         System.out.println(listitem);
          int quantity  = 0 ;
          int key = 0;
         for(Product prod : listproduct)
         {
             for(Item ite : listitem)
             {
                if(ite.getProductId()== prod.getProductId() && ite.getSalesPrice()>prod.getTargetPrice())
                {
                    
                   quantity = quantity + ite.getQuantity();
                }
                
             } 
             quantityStorage.put(key, quantity);
              quantity = 0 ;
             key = key + 1 ;
             
    }
         System.out.println(quantityStorage);
        
         List<Entry<Integer,Integer>> list = new LinkedList<Entry<Integer,Integer>>(quantityStorage.entrySet());
         
         Collections.sort(list, new Comparator<Entry<Integer,Integer>>(){
             
            @Override
            public int compare(Entry<Integer, Integer> t1, Entry<Integer, Integer> t2) {
                return t2.getValue().compareTo(t1.getValue());
            }
             
        
    });
    
         for(int i = 0; i<3 ; i++ )
         {
             System.out.println("Top 3 Best Negotiated products With Quantity and productId's" + " " + list.get(i));
         }
         
         System.out.println("-------------------------------------------------");
         
         
}
    
    public void bestThreeCustomers() {
        Map<Integer, Order> orders = DataStore.getInstance().getOrders();
        Map<Integer, Item> items = DataStore.getInstance().getItems();
        Map<Integer, Product> products = DataStore.getInstance().getProducts();
        Map<Integer, Customer> customers = DataStore.getInstance().getCustomers();
        List<Customer> customerList = new ArrayList<>(customers.values());
       // System.out.println(customerList.get(0).getOrders());
//System.out.println(items);
        int diff;

        for (Item item : items.values()) {
            diff = 0;
            for (Product product : products.values()) {
                if ((product.getProductId() == item.getProductId())) {
                    diff = (item.getSalesPrice() * item.getQuantity())-(product.getTargetPrice() * item.getQuantity()) ;
                }
            }
            item.setDifferencePrice(diff);
        }

        int sumOfDiff;
        for (Customer customer : customers.values()) {
            sumOfDiff = 0;
            for (Order order : customer.getOrders()) {
                if (customer.getCustomerId() == order.getCustomerId()) {
                    for (Item item : order.getItems()) {
                        sumOfDiff += item.getDifferencePrice();
                       // System.out.println(sumOfDiff);
                    }
                }
            }
            customer.setTotalDifference(sumOfDiff);
            // System.out.println(customer.getTotalDifference());
        }
        Collections.sort(customerList, new Comparator<Customer>() {
            public int compare(Customer c1, Customer c2) {
                return Double.compare(c2.getTotalDifference(), c1.getTotalDifference());
            }
        });

        System.out.println("Best 3 customers: ");
        for (int i = 0; i < customerList.size() && i < 3; i++) {
            Customer c = customerList.get(i);
            //System.out.println(c);
            System.out.println("Customer Id= " + c.getCustomerId() + " ,Sum of difference of sales and target price " + c.getTotalDifference());
        }
    }
    
    
    //Q3 added by Kiran
    public void top3BestSalesPerson() {
        System.out.println("Question 3");
        Map<Integer,Integer> bestSales = new HashMap<Integer,Integer>();
        Map<Integer,Product> product = DataStore.getInstance().getProducts();
        List<Product> productValues = new ArrayList<>(product.values());
        //System.out.println(productValues);
        
       Map<Integer,Item> item = DataStore.getInstance().getItems();
       List<Item> itemValues = new ArrayList<>(item.values());
       //System.out.println(itemValues);
       
       Map<Integer,Order> order = DataStore.getInstance().getOrders();
       List<Order> orderValues = new ArrayList<>(order.values());
       //System.out.println(orderValues);
       
       int profit = 0;
       //int count = 0;
       
       for(Item itm : itemValues) {
           
          for(Product prod : productValues){ 
               if(prod.getProductId()== itm.getProductId()){
                   profit = profit+ (itm.getSalesPrice()-prod.getTargetPrice())*itm.getQuantity();
               }
          
          }
           bestSales.put(itm.getSalesId(), profit); 
           profit =0;
           //count++;
       }
       
       System.out.println(bestSales);
      
       Set<Entry<Integer,Integer>> set = bestSales.entrySet();
      List<Entry<Integer,Integer>> list = new ArrayList<Entry<Integer,Integer>>(set);
  Collections.sort(list,new Comparator<Entry<Integer, Integer>>() {
            @Override
            public int compare(Entry<Integer, Integer> o1, Entry<Integer, Integer> o2) {
                return (o2.getValue()).compareTo(o1.getValue());
            }

        }
  
  );

    for(int i = 0;i<list.size() && i<3 ;i++){
        System.out.println("The sales people with more profit "+list.get(i));
    }
    
     System.out.println("--------------------------------------------------------------------------------------");
    System.out.println("Question 4"); 
    //Q4 total revenue  
     
     System.out.println(bestSales.values());
       List<Integer> listitem = new ArrayList<>(bestSales.values());
       //System.out.println(listitem);
        int sum = 0;
        int m = listitem.size();
        for(int i = 0;i<m;i++){
            sum = sum + listitem.get(i);
            
        }
        System.out.println("The total revenue is "+sum);
}   
  



    
    
        //first CALCULATING THE NEW TARGET PRICE
        public void modifiedTarget() {
        Map<Integer, Item> items = DataStore.getInstance().getItems();
        Map<Integer, Product> products = DataStore.getInstance().getProducts();
        Map<Integer, SalesPerson> salesPersons = DataStore.getInstance().getSalesPerson();
        List<Product> productList = new ArrayList<>(products.values());

        double avgSalesPrice = 0;
        int noOfProductsSold = 0;
        int totalSalesPrice = 0;
        double differenceAvgAndTargetOriginal = 0;

        for (Product product : products.values()) {
            totalSalesPrice = 0;
            noOfProductsSold = 0;
            for (Item item : items.values()) {
                if ((product.getProductId() == item.getProductId())) {
                    noOfProductsSold += item.getQuantity();
                    totalSalesPrice += item.getSalesPrice() * item.getQuantity();
                }
            }
            avgSalesPrice = totalSalesPrice / noOfProductsSold;
            product.setAvgSalesPrice(avgSalesPrice);
        }
        System.out.println("Original Data");
        System.out.println("Product Id  |   AverageSales Price  |  Target Price   |    Difference between average and target original");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        for (Product product : products.values()) {
            differenceAvgAndTargetOriginal = product.getAvgSalesPrice()- product.getTargetPrice();
            product.setDiffAvgAndTargetOriginal(differenceAvgAndTargetOriginal);
             
             
             System.out.println(product.getProductId() + "          |          " + product.getAvgSalesPrice()
                    + "         |          " + product.getTargetPrice() + "        |          " + differenceAvgAndTargetOriginal);
            System.out.println("------------------------------------------------------------------------------------------------------------");
            
        }

        Collections.sort(productList, new Comparator<Product>() {
            @Override
            public int compare(Product p1, Product p2) {
                return Double.compare(p2.getDiffAvgAndTargetOriginal(), p1.getDiffAvgAndTargetOriginal());
            }
        });
    }

        
    public void getOriginalAndModifiedData() {
        Map<Integer, Product> products = DataStore.getInstance().getProducts();
        List<Product> productListOriginal = new ArrayList<>(products.values());
        List<Product> productListModified = new ArrayList<>(products.values());

        Collections.sort(productListOriginal, new Comparator<Product>() {
            public int compare(Product p1, Product p2) {
                return Double.compare(p2.getDiffAvgAndTargetOriginal(), p1.getDiffAvgAndTargetOriginal());
            }
        });

        Collections.sort(productListModified, new Comparator<Product>() {
            public int compare(Product p1, Product p2) {
                return Double.compare(p2.getDiffAvgAndTargetModified(), p1.getDiffAvgAndTargetModified());
            }
        });

        

        System.out.println("Modified Data");
        System.out.println("Product Id  |   Average Sales Price  |  Modified Target price    |   Difference b/w Average Sales Price & Target price    |   Error");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        for (Product product : productListModified) {
            System.out.println(product.getProductId() + "          |          " + product.getAvgSalesPrice()
                    + "         |           " + product.getModifiedTargetPrice() + "              |                     " + product.getDiffAvgAndTargetModified()+ "                              | " + product.getError());
            System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------------------");

        }

    }    
        
}
             
         




