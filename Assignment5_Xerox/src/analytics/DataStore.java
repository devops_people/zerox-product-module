/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analytics;

import java.util.HashMap;
import java.util.Map;
import com.assignment5.entities.Customer;
import com.assignment5.entities.Item;
import com.assignment5.entities.Order;
import com.assignment5.entities.Product;
import com.assignment5.entities.SalesPerson;
/**
 *
 * @author Aashita
 */
public class DataStore {
    
    private static DataStore dataStore;
    
    private Map<Integer, Customer> customers;
    private Map<Integer, Item> items;
    private Map<Integer, Order> orders;
    private Map<Integer, Product> products;
    private Map<Integer, SalesPerson> salesPersons;
    
    private DataStore(){
        customers = new HashMap<>();
        items = new HashMap<>();
        orders = new HashMap<>();
        products = new HashMap<>();
        salesPersons = new HashMap<>();   
    }
    
    public static DataStore getInstance(){
        if(dataStore == null)
            dataStore = new DataStore();
        return dataStore;
    }
    
    public static DataStore getDataStore() {
        return dataStore;
    }
    
     public static void setDataStore(DataStore dataStore) {
        DataStore.dataStore = dataStore;
    }

    public Map<Integer, Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(Map<Integer, Customer> customer) {
        this.customers = customers;
    }

    public Map<Integer, Item> getItems() {
        return items;
    }

    public void setItems(Map<Integer, Item> item) {
        this.items = items;
    }

    public Map<Integer, Order> getOrders() {
        return orders;
    }

    public void setOrders(Map<Integer, Order> order) {
        this.orders = orders;
    }

    public Map<Integer, Product> getProducts() {
        return products;
    }

    public void setProducts(Map<Integer, Product> product) {
        this.products = product;
    }

    public Map<Integer, SalesPerson> getSalesPerson() {
        return salesPersons;
    }

    public void setSalesPerson(Map<Integer, SalesPerson> salesPerson) {
        this.salesPersons = salesPersons;
    }
     
     
    
}
